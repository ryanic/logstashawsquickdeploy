# LogstashAWSQuickDeploy

## Overview

### This is just a quick demo for getting a Logstash server up and running in AWS within minutes. After following the steps below, you'll have a Logstash server EC2 instance that will, by default, be listening on 5514/UDP and writing anything ingested to /tmp/logstash.out. Any updates you make or files you add to the configuration directory (/etc/logstash/conf.d) will automatically apply to the pipeline. Have fun!

## Detailed Steps

### 1. Navigate to your AWS console and go to **CloudFormation**.

<img src="https://gitlab.com/ryanic/logstashawsquickdeploy/raw/master/images/console-CF.PNG">

### 2. Under "Create a stack", click on **Create new stack**.

<img src="https://gitlab.com/ryanic/logstashawsquickdeploy/raw/master/images/CF-new.PNG" width="750">

### 3. Under "Choose a template", specify **https://s3-external-1.amazonaws.com/cf-templates-ea4xzo2mbmsf-us-east-1/201903172X-LogstashQuickDeploymnf8n3bxe9** as the S3 template URL and click **Next**. (A copy of the template is located here as logstashcloudformation.json along with the userdata that runs upon boot -- userdata.sh)

<img src="https://gitlab.com/ryanic/logstashawsquickdeploy/raw/master/images/CF-template.PNG" width="900">

### 4. Enter a **Stack name**, choose an **instance type**, choose an **SSH key** (create one in EC2 if one does not exist), (optionally) update the **IP address** for the system that will access this instance via SSH, and click **Next**.

<img src="https://gitlab.com/ryanic/logstashawsquickdeploy/raw/master/images/CF-details.PNG" width="900">

### 5. To give this instance a name to easily identify it, create a new tag with the Key of **Name** and a Value of your choosing, and click **Next**.

<img src="https://gitlab.com/ryanic/logstashawsquickdeploy/raw/master/images/CF-options.PNG" width="900">

### 6. Review your settings and click **Create**.

<img src="https://gitlab.com/ryanic/logstashawsquickdeploy/raw/master/images/CF-review.PNG" width="900">