#!/bin/bash
yum update -y
yum install java -y
mkdir /etc/logstash

cat > /etc/logstash/jvm.options <<EOF
-Xms$(( $(top -n1 -b | grep avail | awk '{print $9}') / 2000 ))m
-Xmx$(( $(top -n1 -b | grep avail | awk '{print $9}') / 2000 ))m
-XX:+UseParNewGC
-XX:+UseConcMarkSweepGC
-XX:CMSInitiatingOccupancyFraction=75
-XX:+UseCMSInitiatingOccupancyOnly
-Djava.awt.headless=true
-Dfile.encoding=UTF-8
-Djruby.compile.invokedynamic=true
-Djruby.jit.threshold=0
-XX:+HeapDumpOnOutOfMemoryError
-Djava.security.egd=file:/dev/urandom
EOF

cat > /etc/logstash/logstash.yml <<EOF
path.data: /var/lib/logstash
config.reload.automatic: true
path.logs: /var/log/logstash
EOF

URL=$(curl https://www.elastic.co/downloads/logstash | grep ">RPM<" | grep -v alpha | cut -d "\"" -f2)

curl -o /tmp/logstash.rpm $URL
rpm -ivh /tmp/logstash.rpm

cat > /etc/logstash/conf.d/test.conf <<EOF
input {
  udp {
    port => 5514
  }
}
output {
  file {
    path => "/tmp/logstash.out"
  }
}
EOF

systemctl enable logstash
systemctl start logstash